({
    
    handleSuccess: function(component, event, helper) {
        //alert(JSON.stringify(component.get("v.selectedContacts")));
        var params = event.getParams(); 
        var recordId = params.response.id; 
        var action = component.get("c.updateContacts");
        
        action.setParams({"contacts":component.get("v.selectedContacts"),"accId":recordId});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                if(response.getReturnValue()){
                    var navservice = component.find("navService");        
                    var pageReference = {
                        "type": 'standard__recordPage',         
                        "attributes": {              
                            "recordId":recordId,
                            "actionName": "view",               
                            "objectApiName":"Account"              
                        }        
                    };
                    var defaultUrl = "#";
                    navservice.generateUrl(pageReference)
                    .then($A.getCallback(function(url) {
                        component.set("v.url", url ? url : defaultUrl);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            title: "Success!",
                            type:"success",
                            message:"This is a required message",
                            messageTemplate: 'Account has been Created successfully.Click {1} to view Order Details',
                            messageTemplateData: ['Salesforce', {
                                url:component.get("v.url"),
                                label: 'here',
                            }
                                                 ]
                        });
                        toastEvent.fire();
                    })
                         );
                    
                    
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                alert(JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
        
    },
    handlecheckbox: function(component, event, helper) {
        if(component.get("v.Billing_Same_as_Shipping")){
            
            
            
            
            component.find("shippingStreet").set("v.value", component.find("billingStreet").get("v.value"));
            
            component.find("shippingCity").set("v.value", component.find("billingCity").get("v.value"));
            component.find("shippingCountry").set("v.value", component.find("billingCountry").get("v.value"));
            
            component.find("shippingState").set("v.value", component.find("billingState").get("v.value"));     
            component.find("shippingPostalCode").set("v.value", component.find("billingPostalCode").get("v.value"));
            
        }
        else{
            component.find("shippingStreet").set("v.value", '');
            
            component.find("shippingCity").set("v.value", '');
            component.find("shippingCountry").set("v.value",'');
            
            component.find("shippingState").set("v.value", '');     
            component.find("shippingPostalCode").set("v.value", '');
        }
    },
    loadContacts: function(component, event, helper) {
        var modalBody;
        $A.createComponent("c:loadContacts", {},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "Please pick atleast 2 contacts to create Account",
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: "mymodal",
                                       closeCallback: function() {
                                           
                                       }
                                   })
                               }
                           });
    },
    ApplicationEvent: function(component, event, helper) {
        var selectedContacts = event.getParam("SelectedContacts");
        component.set("v.selectedContacts",selectedContacts);
        
    },
    
})