import { LightningElement ,api} from 'lwc';

export default class EventWithData extends LightningElement {
@api parentValue;
selecthandler(event){
    event.preventDefault();

    // Creates the event with the contact ID data.
    const selectedEvent = new CustomEvent('selected', { detail: this.parentValue.Id });

    // Dispatches the event.
    this.dispatchEvent(selectedEvent);
}
    
}