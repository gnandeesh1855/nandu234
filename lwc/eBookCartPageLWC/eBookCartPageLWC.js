import { LightningElement ,wire,track,api } from 'lwc';
import { updateRecord ,deleteRecord } from 'lightning/uiRecordApi';
import ID_FIELD from "@salesforce/schema/eBooks_Cart_Items__c.Id";
import Quantity_FIELD from "@salesforce/schema/eBooks_Cart_Items__c.Quantity__c";
import TOTALPRICE_FIELD from "@salesforce/schema/eBooks_Cart_Items__c.Total_Price__c";
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CartImg from '@salesforce/resourceUrl/CartImg';


export default class EBookCartPageLWC extends NavigationMixin(LightningElement) {
    @api cartItemsList;
   // @wire(getCartDetails) cartItemsList;
    CartImgurl=CartImg;
    
  get cartlist(){
      if(this.cartItemsList){
          console.log(this.cartItemsList);
          return this.cartItemsList.length;
      }
      else{
          return 0;
      }
  }
  get isList(){
    if(this.cartItemsList && this.cartItemsList.length>0){
        return true;
    }
    else{
        return false;
    }
  }

    onQuantityChange(event){
        var item=event.target.name;
        var qty=item.Quantity__c;
        var totalPrice=0;
        /* eslint-disable no-console */
      console.log(item.Quantity__c);
        if(event.target.value=='sub'){
           qty=qty-1;
        }
        else if(event.target.value=='add'){
            qty=qty+1;
        }
        if(qty>0){
            totalPrice= (item.Book_Name__r.Price__c)*qty;
        
        const fields = {};
        fields[ID_FIELD.fieldApiName] = item.Id;
        fields[Quantity_FIELD.fieldApiName] = qty;
        fields[TOTALPRICE_FIELD.fieldApiName] = totalPrice;
        const recordInput = { fields };
        updateRecord(recordInput)
                .then(() => {
                  
                    const selectEvent = new CustomEvent('mycustomevent');
                    this.dispatchEvent(selectEvent);
                })
                .catch(error => {
                   alert(error.body.message);
                });
            }
            else if(qty<=0){
                deleteRecord(item.Id)
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            
                            type:'error',
                            message: item.Book_Name__r.Name + ' deleted from cart',
                            variant: 'error'
                        })
                    );
                  
                    const selectEvent = new CustomEvent('mycustomevent');
                   this.dispatchEvent(selectEvent);
            
                    
                })
                .catch(error => {
                   
                });
        }
            }

            deletecartItem(event){
                var name=''
                // var id=event.currentTarget.id.substring(0,event.currentTarget.id.indexof("-"));
                this.cartItemsList.find(cart =>{
                 if(cart.Id==event.currentTarget.dataset.id){
                    name=cart.Book_Name__r.Name;
                    
                
                 }
             });
                
               deleteRecord(event.currentTarget.dataset.id)
               .then(() => {
             
            this.dispatchEvent(
                    new ShowToastEvent({
                        type:'error',
                        message: name + ' deleted from cart',
                        variant: 'error'
                    })
                );
              
                   //return refreshApex(this.cartItemsList);
                   
                   const selectEvent = new CustomEvent('mycustomevent');
                   this.dispatchEvent(selectEvent);
                   
               })
               .catch(error => {
                 
               });
            }
   
    naviagteToHomePage(event){
         
        const selectEvent = new CustomEvent('mycustomevent1');
        this.dispatchEvent(selectEvent);
        // this[NavigationMixin.Navigate]({
        //     type: 'standard__navItemPage',
        //     attributes: {
        //         apiName: 'E_Book_LWC',
        //     },
            
        // });
        //    
    }

            

}