import { LightningElement,wire } from 'lwc';
import getcars from '@salesforce/apex/carController.getCars';

import {subscribe, MessageContext, publish,unsubscribe} from 'lightning/messageService';
import CARS_IMPORT_MESG from '@salesforce/messageChannel/CarsFiltered__c';
import CARS_SELECTED_DATA from '@salesforce/messageChannel/CarsSelected__c';


export default class CarTileList extends LightningElement {
    cars=[]
    error
    message
    filters= {};
    carFilterSubscription
    car;

    @wire(MessageContext)
    messageContext;

    @wire(getcars,{filters:'$filters'})
    carsHandler({ error, data }) {
      if (data) {
        console.log('Data', data);
        this.cars = data;
      } else if (error) {
        console.error('Error:', error);
        this.error = error;
      }
    }

  /*  @wire(MessageContext)
    messageContext; */

    connectedCallback() {
      
      console.log('connectedCallback');
      this.subscribeHandler()
    }

    subscribeHandler(){
      console.log('subscribeHandler');
      this.carFilterSubscription = subscribe(this.messageContext, CARS_IMPORT_MESG, (message)=> this.handleFilterChanges(message))
    }
    handleFilterChanges(message){
      
      console.log('handleFilterChanges');
      this.filters ={...message.filters}
  }

  handleSelectedId(event){
    console.log(event.detail.carId)
    console.log(event.detail.carMessage)
    this.car = event.detail;
    this.publishSelectedCarData()
  }

  publishSelectedCarData(){
    publish(this.messageContext, CARS_SELECTED_DATA, {
      car:this.car
  })
  }

  
  disconnectedCallback(){
    unsubscribe(this.carFilterSubscription)
    this.carFilterSubscription = null
}

}