import { LightningElement } from 'lwc';
export default class LifeCycleChild extends LightningElement {
    constructor(){
        super()
        console.log('Child Constructor');
    }
    connectedCallback() {
        console.log('Child Connected callback');
    }
    renderedCallback(){
        console.log('Child rendered callback');
    }
    disconnectedCallback(){
         alert('Child Disconnectcallback')
    }
}