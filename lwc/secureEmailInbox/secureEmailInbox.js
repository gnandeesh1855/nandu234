import { LightningElement, api, wire } from 'lwc';
import externalCSS from '@salesforce/resourceUrl/externalCSS';
import { loadStyle } from 'lightning/platformResourceLoader';
import getEmailList from '@salesforce/apex/SecureEmailInboxController.getEmailList';
import getRecordSpecificEmailList from '@salesforce/apex/SecureEmailInboxController.getRecordSpecificEmailList';


export default class SecureEmailInbox extends LightningElement {

    @api recordId;
    error;
    emailList = [];

    renderedCallback() {
        Promise.all([
            loadStyle( this, externalCSS )
        ])
        .then(() => {
            console.log( 'Files loaded' );
        })
        .catch(error => {
            console.log( error.body.message );
        });
    }

    connectedCallback(){
        if(this.recordId){
            getRecordSpecificEmailList({recordId: this.recordId})
            .then(result => {
                this.prepareEmailList(result);    
            })
            .catch(error => {
                this.error = error;
                console.error(this.error);
                this.emailList = undefined;
            })
        }
        else {
            getEmailList()
            .then(result => {
                this.prepareEmailList(result);    
            })
            .catch(error => {
                this.error = error;
                console.error(this.error);
                this.emailList = undefined;
            })    
        }
    }

    prepareEmailList(data) {
        var tempemailList = [];
        data.listofStanderedEmail.forEach(currentItem => {
            tempemailList.push(currentItem);
        });
        data.listOfCustomEmail.forEach(currentItem => {
            let emailData = {};
            emailData.Subject = currentItem.cmsecureemail__Subject__c;
            emailData.FromName = currentItem.cmsecureemail__FromAddress__c;
            emailData.FromAddress = currentItem.cmsecureemail__FromAddress__c;
            emailData.ToAddress  = currentItem.cmsecureemail__Reply_to__c;
            emailData.Contact = currentItem.cmsecureemail__Contact__c;
            emailData.HtmlBody = currentItem.cmsecureemail__HtmlBody__c;
            emailData.TextBody = currentItem.cmsecureemail__TextBody__c;
            emailData.BccRecipients = currentItem.cmsecureemail__BccRecipients__c;
            emailData.Id = currentItem.Id;
            emailData.MessageDate = currentItem.CreatedDate;
            emailData.recepients = currentItem.cmsecureemail__Recipients__c;
            emailData.recordType = (currentItem.RecordType.Name == 'Outbound' ? true : false);
            emailData.isCustomEmail = true;
            tempemailList.push(emailData);
        });
        tempemailList.sort(function(a,b){
            return new Date(b.MessageDate) -  new Date(a.MessageDate)
        });
        this.emailList = tempemailList;
    }
}