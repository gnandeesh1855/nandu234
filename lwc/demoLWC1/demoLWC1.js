import { LightningElement,api } from 'lwc';
export default class DemoLWC1 extends LightningElement {
    carList = ['Audi','Merc','Proche','Lambo','RR'];

    ceoList = [
        {
            id:'1',
            company:'Google',
            CEO:'Pichai'
        },{
            id:2,
            company:'Apple',
            CEO:'Tim'
        },{
            id:3,
            company:'Mozilla',
            CEO:'Random'
        }
    ];

}