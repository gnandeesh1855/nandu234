import { LightningElement ,api,wire,track} from 'lwc';
import getAccounts1   from '@salesforce/apex/AccountsController.getAccounts1';
import getAccounts from '@salesforce/apex/AccountsController.getAccounts';
import { getRecord } from 'lightning/uiRecordApi';
import Id from '@salesforce/user/Id';
export default class MyFirstLWC extends LightningElement {
    @api name="Lightning web component";
    @track searchvalue1="";
    @api userId=Id;
    @api recordId;
    @track record;
    @track error;
    @wire(getAccounts,{ searchvalue: '$searchvalue1' }) accounts;
    @wire(getAccounts1,{ searchvalue: '$searchvalue1' }) accounts1;
 
     @wire(getRecord, { recordId: '$recordId', fields: ['Account.Name'] })
     wiredAccount({ error, data }) {
         if (data) {
             this.record = data;
              /* eslint-disable no-console */
              console.log('typeof record ' +typeof this.record);
          console.log('RECORD ---> '+ JSON.stringify(this.record));
             this.error = undefined;
         } else if (error) {
             this.error = error;
             this.record = undefined;
         }
     }
     get name() {
         return this.record.fields.Name.value;
     }
     
    handleSearchValueChange(event){
        this.searchvalue1 = event.target.value;
         /* eslint-disable no-console */
         console.log('typeof this.accounts.data ---> '+typeof this.accounts.data);
         console.log('typeof this.accounts ---> '+typeof this.accounts);
         // console.log(this.searchvalue1);
          console.log('accounts1 ---> '+this.accounts);
     }

}