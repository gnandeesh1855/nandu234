import { LightningElement } from 'lwc';
export default class QuizApp extends LightningElement {

     selected = {};
  //  allNotSelected = true;
    correctanswers = 0;
    issubmitted = false;
    get allNotSelected(){
        return !(this.Questions.length === Object.keys(this.selected).length);
    }
    get allSubmittedResult(){
        return `${this.correctanswers === this.Questions.length ? 'slds-text-color_success' : 'slds-text-color_error'}  `;
    }
    Questions =[
        {
            id:'Question1',
            Question:'Which tool should an administrator use to identify and fix potential session vulnerabilities?',
            answers:{
                a: 'Setup Audit Trail',
                b: 'Field History Tracking',
                c: 'Organization-Wide Defaults'
            },
            correctanswer:'c'
        },
        {
            id:'Question2',
            Question:'The administrator at Cloud Kicks is trying to debug a screen flow that creates contacts. One of the variables in the flow is missing on the debug screen?',
            answers:{
                a: 'Approval Process',
                b: 'Validation Rule',
                c: 'Flow Builder'
            },
            correctanswer:'c'
        },
        {
            id:'Question3',
            Question:'An administrator at Universal Container needs an automated way to delete records based on field values?',
            answers:{
                a: 'Workflow',
                b: 'Automation Studio',
                c: 'Process Builder'
            },
            correctanswer:'b'
        }
        
    ];

     onOptionSelect(event){
            console.log(event.target.value);
            console.log(event.target.name);
            const {name,value} = event.target;
            console.log(event.target);
            console.log({name,value});
            this.selected = {...this.selected, [name]:value};
            // alert('onoption'+this.selected);
            console.log(this.selected);
     }

     onSubmit(event){
            console.log('onSubmit');
            event.preventDefault();
            let correct = this.Questions.filter(item=>this.selected[item.id] === item.correctanswer );
            this.correctanswers = correct.length;
            this.issubmitted = true;
            console.log('this.correct ', correct );
            console.log('this.correctanswers ', this.correctanswers );

     }

     onReset(event){
         console.log(this.selected);
         
         this.selected = {};
       //  alert('OnReset'+this.selected);
         this.correctanswers = 0;
         this.issubmitted = false;
         console.log(this.selected);
     }

}