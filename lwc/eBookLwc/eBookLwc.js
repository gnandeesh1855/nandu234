import { LightningElement,wire,track } from 'lwc';
import getEbooksList from '@salesforce/apex/eBookApex.getEbooksList';
import getEbooks from '@salesforce/apex/eBookApex.getEbooks';
import getCartDetails from '@salesforce/apex/eBookApex.getCartDetails';
import addToCart from '@salesforce/apex/eBookApex.addToCart';
import { refreshApex } from '@salesforce/apex';
import getCartId from '@salesforce/apex/eBookApex.getCartId';
import {CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { updateRecord ,createRecord} from 'lightning/uiRecordApi';
import ID_FIELD from "@salesforce/schema/eBooks_Cart_Items__c.Id";
import Quantity_FIELD from "@salesforce/schema/eBooks_Cart_Items__c.Quantity__c";
import TOTALPRICE_FIELD from "@salesforce/schema/eBooks_Cart_Items__c.Total_Price__c";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import eBookImg from '@salesforce/resourceUrl/eBookImg';


export default class EBookLwc  extends NavigationMixin(LightningElement) {
    @track searchValue='';
    @track ebooksList;
    @track showModal=false;
    @track viewId='';
    @track eBooksandCartItems;
   @track cartItemsList='';
   @track showCart=false;
   @track connected=false;
   eBookImgurl=eBookImg;
    error;

   // @wire(getCartDetails) cartItemsList;

   handleCustomEvent(){
       console.log('handle custom event');
       return refreshApex(this.eBooksandCartItems)
   }
   handleCustomEvent1(){
    this.showCart=false;
    
}



    @wire(getEbooksList,{value:'$searchValue'})
    wiredData(result) {
        this.eBooksandCartItems =result;
      if (result.data) {
            
        this.ebooksList= result.data.ebooksList;
        this.cartItemsList=result.data.cartItemsList;
        console.log('getEbOOKSLIST :::',JSON.stringify(this.eBooksandCartItems) ,this.cartItemsList);
        this.handleAddRemoveButtonLabel();
        this.error=undefined;
       
      } else if (result.error) {
       
      console.log(result.error);
        this.ebooksList=undefined;
        this.cartItemsList=undefined;
      }
    }

    handleAddRemoveButtonLabel(){
        let optionsList = this.ebooksList;
        let booknewarr = [];
        optionsList.forEach(record=>{
        
    let eachRecord ={};
        
    eachRecord.Id = record.Id;
        
    eachRecord.Description__c= record.Description__c;
        
eachRecord.Price__c = record.Price__c;
        
    eachRecord.Name= record.Name;
        eachRecord.Book_Id__c= record.Book_Id__c;
        
   
        
        if(this.cartItemsList==undefined || this.cartItemsList.length<0){
            
        
        eachRecord.isAddedToCart=false;
        }
        
        else{
        
    let selectedItem = this.cartItemsList.filter((el) => el.Book_Name__c==record.Id);
        
if(selectedItem!=undefined && selectedItem && selectedItem.length>0){
        eachRecord.isAddedToCart =true;
        
}else{
        
eachRecord.isAddedToCart = false;
        

        
    }
        
        }
        
    booknewarr.push(eachRecord);
        
})
    this.ebooksList = [...booknewarr];
        
//console.log('newl created books :::',JSON.stringify(this.ebooksList));
    }
    
   
   
   
    

    get noOfCartItems(){
        
        if(this.cartItemsList){
           
           
            return this.cartItemsList.length;
            
            
        }
        else{
          return 20;
        }
    }
    
    onSearchValueChange(event){
        this.searchValue=event.target.value;
    }
  
  
    addtocart(event){
        
        
      var Id=event.target.name;
   

   
            
               
                for(let i=0; i<this.ebooksList.length; i++){
                         
                  if(this.ebooksList[i].Id==Id){
                    getCartId()
                    .then(result => {
                         
                        var fields = {'Quantity__c' : 1, Total_Price__c:this.ebooksList[i].Price__c, Book_Name__c :this.ebooksList[i].Id ,Cart_Name__c:result};
                        // Record details to pass to create method with api name of Object.
                        var objRecordInput = {'apiName' : 'eBooks_Cart_Items__c', fields};
                        // LDS method to create record.
                        createRecord(objRecordInput).then(response => {
                            this.dispatchEvent(
                                new ShowToastEvent({
                                    
                                    type:'success',
                                    message: this.ebooksList[i].Name + ' successfully added to cart',
                                    variant: 'success'
                                })
                            );
                            
                           // return refreshApex(this.cartItemsList);
                           return refreshApex(this.eBooksandCartItems);
                        }).catch(error => {
                            /* eslint-disable no-console */
                            
                        });
                    })
                    .catch(error => {
                        /* eslint-disable no-console */
                        console.log('result'+result);
                    });
            }
                    
                          }
                      
                  
              }
               
    

    
    showInfo(event){
        event.preventDefault();
        this.showModal = true;
        this.viewId=event.target.name;


    }
    closeModal() {
        // Setting boolean variable to false, this will hide the Modal
        this.showModal = false;
    }

    onClickCart(event){
        this.showCart=true;
        // var compDefinition = {
        //     componentDef: "c:eBookCartPageLWC",
            
            
        // };
        // // Base64 encode the compDefinition JS object
        // var encodedCompDef = btoa(JSON.stringify(compDefinition));
        // this[NavigationMixin.Navigate]({
        //     type: 'standard__webPage',
        //     attributes: {
        //         url: '/one/one.app#' + encodedCompDef
        //     }
        // });
    }
}