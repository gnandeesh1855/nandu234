import { LightningElement, api, track, wire } from 'lwc';
import getEmails from '@salesforce/apex/SecureEmailItemController.getEmails';
import sendMail from '@salesforce/apex/SecureEmailItemController.sendMail';
import sendReplyAllMail from '@salesforce/apex/SecureEmailItemController.sendReplyAllMail';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import incomingEmailLogo from '@salesforce/resourceUrl/incomingEmailLogo';
import outgoingEmailLogo from '@salesforce/resourceUrl/outgoingEmailLogo';

export default class SecureEmailItem extends NavigationMixin(LightningElement) {

    @api email;
    emails;
    error;
    error1;
    isReplyModalVisible = false;
    isReplyAllModalVisible = false;
    replyText;
    replyAllText;
    selectedOption;
    userName;
    userEmail;
    incomingLogo = incomingEmailLogo;
    outgoingLogo = outgoingEmailLogo;

    connectedCallback() {
        getEmails()
            .then(result => {
                console.log('result', result);
                this.emails = result.listofOrgWideEmails;
                console.log('emails', this.emails);
                this.userName = result.currentUserName;
                this.userEmail = result.currentUserEmail;
                //this.prepareEmailList(result);
                this.selectedOption = this.userEmail;
                console.log('connected this.seleopt:  ',this.selectedOption);
            })
            .catch(error => {
                this.error = error;
                console.error(this.error);
                this.emails = undefined;
            })
            
    }

    changeHandler(event) {
        const field = event.target.name;
        if (field === 'optionSelect') {
            this.selectedOption = event.target.value;
            console.log('selectedOption', this.selectedOption);
        }
    }

    get initials() {
        if (this.email.FromName) {
            return this.generateInitials(this.email.FromName);
        }
        else if (this.email.From_Name__c) {
            return this.generateInitials(this.email.From_Name__c);
        }
    }

    get fromName() {
        if (this.email.FromName) {
            return this.email.FromName;
        }
        else if (this.email.From_Name__c) {
            return this.email.From_Name__c;
        }
        else if (this.email.cmsecureemail__FromAddress__c) {
            return this.email.cmsecureemail__FromAddress__c;
        }
    }

    generateInitials(name) {
        let rgx = new RegExp(/(\p{L}{1})\p{L}+/, 'gu');

        let initials = [...name.matchAll(rgx)] || [];

        initials = (
            (initials.shift()?.[1] || '') + (initials.pop()?.[1] || '')
        ).toUpperCase();
        return initials;
    }

    handleTileClick(event){
        console.log('In method......');
        console.log('Id', this.email.Id);
        event.preventDefault();
        event.stopPropagation();
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.email.Id,
                //objectApiName: 'namespace__ObjectName',
                actionName: 'view'
            }
        });
    }

    handleMenuSelect(event) {
        const selectedItemValue = event.detail.value;
        if (selectedItemValue == 'reply') {
            this.openReplyModal();
        }
        else if (selectedItemValue == 'replyAll') {
            this.openReplyAllModal();
        }
    }

    openReplyModal() {
        this.isReplyModalVisible = true;
        const separator = '<br/><br/><br/>--------------------------------------------------<br/>';
        this.replyText = separator + this.email.HtmlBody;
    }

    openReplyAllModal() {
        this.isReplyAllModalVisible = true;
        const separator = '<br/><br/><br/>--------------------------------------------------<br/>';
        this.replyAllText = separator + this.email.HtmlBody;
    }

    handleInputChange(event) {
        this.replyText = event.target.value;
    }

    handleReplyAllInputChange(event){
        this.replyAllText = event.target.value;
    }

    closeReplyModal() {
        this.isReplyModalVisible = false;
        this.isReplyAllModalVisible = false;
    }

    sendEmail() {
        sendMail({
            Subject: this.email.Subject,
            FromName: this.email.FromName,
            FromAddress: this.selectedOption,
            ToAddress: this.selectedOption,
            Contact: this.email.Contact,
            HtmlBody: this.replyText,
            TextBody: this.email.TextBody,
            bccRecepients: this.email.bccRecepients,
            Id: this.email.Id,
            MessageDate: this.email.MessageDate,
            recepients: this.email.FromAddress,
            recordType: this.email.recordType
        })
            .then(result => {
                console.log('result', result);
                const evt = new ShowToastEvent({
                    title: 'Success',
                    message: 'Email Sent Successfully',
                    variant: 'success'
                });
                this.dispatchEvent(evt);
            })
            .catch(error => {
                this.error1 = error;
                console.error(this.error1);
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: 'Email Not Sent',
                    variant: 'error'
                });
                this.dispatchEvent(evt);
            })

        this.isReplyModalVisible = false;
    }

    sendReplyAllEmail() {
        sendReplyAllMail({
            Subject: this.email.Subject,
            FromName: this.email.FromName,
            FromAddress: this.selectedOption,
            ToAddress: this.selectedOption,
            Contact: this.email.Contact,
            HtmlBody: this.replyAllText,
            TextBody: this.email.TextBody,
            bccRecepients: this.email.BccRecipients,
            Id: this.email.Id,
            MessageDate: this.email.MessageDate,
            recepients: this.email.FromAddress,
            recordType: this.email.recordType
        })
            .then(result => {
                console.log('result', result);
                const evt = new ShowToastEvent({
                    title: 'Success',
                    message: 'Email Sent Successfully',
                    variant: 'success'
                });
                this.dispatchEvent(evt);
            })
            .catch(error => {
                this.error1 = error;
                console.error(this.error1);
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: 'Email Not Sent',
                    variant: 'error'
                });
                this.dispatchEvent(evt);
            })

        this.isReplyAllModalVisible = false;
    }   

}