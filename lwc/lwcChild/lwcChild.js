import { LightningElement,api } from 'lwc';

export default class ChildComponentLwc extends LightningElement {
    @api childValue;
    @api count=0;
    handleChange(event){
        this.childValue=event.target.value;
    }
    @api
    increment(){
this.count+=1;
    }
}