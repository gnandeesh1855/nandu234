import { LightningElement,wire } from 'lwc';
import NAME_FIELD from '@salesforce/schema/Car__c.Name';
import PICTURE_URL_FIELD from '@salesforce/schema/Car__c.Picture_URL__c';
import CATEGORY_FIELD from '@salesforce/schema/Car__c.Name';
import MAKE_FIELD from '@salesforce/schema/Car__c.Make__c';
import MSRP_FIELD from '@salesforce/schema/Car__c.MSRP__c';
import FUEL_FIELD from '@salesforce/schema/Car__c.Fuel_Type__c';
import SEATS_FIELD from '@salesforce/schema/Car__c.seats__c';
import CONTROL_FIELD from '@salesforce/schema/Car__c.Control__c';
import { NavigationMixin } from 'lightning/navigation';
import CAR_OBJECT from '@salesforce/schema/Car__c';
import {subscribe, MessageContext, unsubscribe} from 'lightning/messageService';
import CARS_SELECTED_DATA from '@salesforce/messageChannel/CarsSelected__c';

import { getFieldValue } from 'lightning/uiRecordApi';

export default class CarCard extends NavigationMixin(LightningElement) {
    categoryField = CATEGORY_FIELD;
    makeField = MAKE_FIELD;
    msrpField = MSRP_FIELD;
    fuelField = FUEL_FIELD;
    seatsField = SEATS_FIELD;
    controlField = CONTROL_FIELD;
    recordid ;
    carName;
    carPicture;
    carSelectedSubscription
    @wire(MessageContext)
    messageContext;

    handleOnLoad(event){
        const {records} = event.detail;
        const recordData = records[this.recordid];
        this.carName = getFieldValue(recordData, NAME_FIELD);
        this.carPicture = getFieldValue(recordData, PICTURE_URL_FIELD);
    }

    connectedCallback(){
        this.subscribeHandler()
    }

    subscribeHandler(){
        this.carSelectedSubscription = subscribe(this.messageContext, CARS_SELECTED_DATA, (message)=> this.handleFilterChanges(message))
    }

    handleFilterChanges(message){
        console.log('---');
        console.log(message);
        this.recordid = message.car.carId;
    }

    disconnectedCallback(){
        unsubscribe(this.carSelectedSubscription)
        this.carSelectedSubscription = null
    }

    handleNavigation(){
        this[NavigationMixin.Navigate]({
            type:'standard__recordPage',
            attributes:{
                recordId: this.recordid,
                objectApiName:CAR_OBJECT.objectApiName,
                actionName:'view'
            }
        })
    }
}