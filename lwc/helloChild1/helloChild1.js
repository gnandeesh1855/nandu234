import { LightningElement } from 'lwc';
export default class HelloChild1 extends LightningElement {
    clickHandler(){
        console.log('onchildclick')
        const eventt = new CustomEvent('close',{
            bubbles:true,
            detail: {
                msg: 'Modal CLosed'
            }
        });
        this.dispatchEvent(eventt);
    }

    clickHandler1(){
        console.log('click hanlder 1');
    }
    clickHandler2(){
        console.log('click hanlder 2');
    }
}