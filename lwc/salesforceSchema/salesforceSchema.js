import { LightningElement ,api,wire,track } from 'lwc';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import ACCOUNT_NAME_FIELD from '@salesforce/schema/Account.Name';
import { getRecord } from 'lightning/uiRecordApi';
/* You can specify up to three relationship fields, which results in four objects
 and the field being referenced.
 For example, Opportunity.Account.CreatedBy.LastModifiedById returns 
 4 levels of spanning fields.*/
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import AccountName from '@salesforce/schema/Account.Name'
import AccountNumber from '@salesforce/schema/Account.AccountNumber';
import AccountSource_Field from '@salesforce/schema/Account.AccountSource';
const namefields = [AccountName,AccountNumber];

export default class SalesforceSchema extends LightningElement {
    @api recordId;
    @api recordTypeId;
    @api value;
   @wire(getRecord, { recordId: '$recordId', fields: namefields })
    record1;
    get Name() {
        return  this.record1.data.fields.Name.value;
    }

    get AccNumber() {
        return this.record1.data.fields.AccountNumber.value;
    }



    @wire(getRecord, { recordId: '$recordId', fields: namefields})
    wiredAccount({ error, data }) {
        if (data) {
            this.record = data;
            this.recordTypeId=data.recordTypeId;
             /* eslint-disable no-console */
             console.log('typeof record ' +typeof this.record);
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.record = undefined;
        }
    }
    @wire(getPicklistValues, { recordTypeId: '$recordTypeId', fieldApiName: AccountSource_Field })
    salutationValues;
    
    get salutations() {
        let salutationOptions = [];
         /* eslint-disable no-console */
         console.log("saluationvalues-->" );
         console.log(this.salutationValues);
         if(this.salutationValues.data){
             /* eslint-disable no-console */
             console.log("into if");
            Object.entries(this.salutationValues.data.values).forEach(val => {
                /* eslint-disable no-console */
                let values = val[1];
                console.log("val[1]");
                console.log(val[1]);
                salutationOptions.push({ 'label': values.label, 'value': values.value});
            }); 
            return salutationOptions;
         }
       
    }

    handleChange(event) {
        this.value = event.detail.value;
    }


    
}