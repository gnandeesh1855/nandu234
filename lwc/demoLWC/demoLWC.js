import { LightningElement,api,track } from 'lwc';

export default class DemoLWC extends LightningElement {
    fullname = 'Salesforce';
    num1 = 20;
    num2 = 40;
    isVisible = false;
    title = 'Aura'
     details = {
        company: "Wipro",
        city:'Australia'
    }

    changeHandler(event){
        this.title = event.target.value;
    }

    ontrack(event){
        this.details = {...this.details, "city" : event.target.value} ;
    }

    get fullnamechange(){
        return this.fullname+ 'nandu';
    }
    get sumOfNum(){
        return this.num1+ this.num2;
    }

    onGetters(event){
        this.fullname = event.target.value;
    }

    callHandleButton(){
        this.isVisible = true;
    }

    //falsy values in JS

    // x= 0, false, null, undefined, '',""

}