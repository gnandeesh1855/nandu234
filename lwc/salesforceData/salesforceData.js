import { LightningElement ,track,wire,api} from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import getAccounts from '@salesforce/apex/AccountsController.getAccounts'

export default class SalesforceData extends LightningElement {
  @track accounts;
  @track searchValue='';
  @track error;
  @api input="input value";
  @track uk=0;
  
  @wire(getAccounts,{searchvalue :'$searchValue'})
  wiredAccount({error,data}){
      if(data){
         
          this.accounts=data;
            /* eslint-disable no-console */
            console.log("hi in data")
            console.log(this.accounts);
      }
      else if(error){
          this.error=error;
            /* eslint-disable no-console */
            console.log('hi in error');
            console.log(this.error);
      }
  }
  handleChange(event){
      const name=event.target.name;
      for(var i=0; i < this.accounts.length;i++){
          if(this.accounts[i].Id==name){
              this.accounts[i].Id=name;
              this.uk=i;
                 /* eslint-disable no-console */
            console.log('this.uk '+ this.uk);
              break;
          }
      }
  }
  handleChange1(event){
      const name=event.target.name;
      if(name=="input"){
          this.input=event.target.value;
      }
      else if(name=="searchValue")
      this.searchValue=event.target.value;
      
  }
}