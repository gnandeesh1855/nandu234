import { LightningElement,api } from 'lwc';

export default class CarTile extends LightningElement {
    @api car={};

    handleClick(){
        this.dispatchEvent(new CustomEvent('selected',{
            detail:
                {
                    carId:this.car.Id,
                    carMessage: 'Car selected'
                }
        }))
    }
}