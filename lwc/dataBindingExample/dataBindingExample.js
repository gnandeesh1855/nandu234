import { LightningElement } from 'lwc';

export default class DataBindingExample extends LightningElement {


    firstName = '';
    lastName = '';
    fullName='';

    handleChange(event) {
        const field = event.target.name;
        if (field === 'firstName') {
            this.firstName = event.target.value;
        } else if (field === 'lastName') {
            this.lastName = event.target.value;
        }
        this.fullName=this.firstName+' '+ this.lastName;
    }

    get uppercasedFullName() {
        return `${this.firstName} ${this.lastName}`.toUpperCase();
    }
}