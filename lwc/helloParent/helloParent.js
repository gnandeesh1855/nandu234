import { LightningElement } from 'lwc';
export default class HelloParent extends LightningElement {
    percentage=0;
    carouselImages = [
        {
            src : "https://www.lightningdesignsystem.com/assets/images/carousel/carousel-01.jpg",
            header : "First Card",
            description : "First card description.",
            alternativetext : "First card accessible description.",
            href : "https://www.example.com"
        },
        {
            src :"https://www.lightningdesignsystem.com/assets/images/carousel/carousel-01.jpg",
            header : "Second Card",
            description : "Second card description.",
            alternativetext : "Second card accessible description.",
            href : "https://www.example.com"
        },
        {
            src : "https://www.lightningdesignsystem.com/assets/images/carousel/carousel-01.jpg",
            header : "Third Card",
            description : "Third card description.",
            alternativetext : "Third card accessible description.",
            href : "https://www.example.com"
        },
    ];

    handleClick(){
        console.log('status', this.percentage);
        this.percentage += 10;
        console.log('status', this.percentage);
    }
    handleBtn(event){
        this.percentage = event.target.value;
    }

    handleReset(event){
        event.target.value = 25;
        this.template.querySelector('c-hello-child-component').changeHandler(event);
    }
    renderedCallback(){
        console.log('renderedcallback');
    }
}