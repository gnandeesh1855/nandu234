import { LightningElement ,api, wire, track} from 'lwc';
import externalCSS from '@salesforce/resourceUrl/externalCSS';
import { loadStyle } from 'lightning/platformResourceLoader';
import getEmailList from '@salesforce/apex/emailInbox.getEmailList';
import updateReplyEmail from '@salesforce/apex/emailInbox.updateReplyEmail';
export default class emailInboxComponent extends LightningElement {
    @track error;
    @track emailList;
    @track message;
    @track selectedEmail;
    @track isReplyClick;
    @track isForwardClick;
    @track textareaReply;
    @track isREReplyClick;

    @track isModalOpen =false;
    
    
    closeModal() {
        // to close modal set isModalOpen track value as false
        this.isModalOpen =false;
        this.isReplyClick = false;
        this.isForwardClick = false;
    }

    renderedCallback() {
        Promise.all([
            loadStyle( this, externalCSS )
        ])
        .then(() => {
            console.log( 'Files loaded' );
        })
        .catch(error => {
            console.log( error.body.message );
        });
    }

    @wire(getEmailList)
        wiredAccounts({
        error,
        data
    }) {
        if (data) {
            
            var tempemailList = [];
            data.listofStanderedEmail.forEach(currentItem => {
                tempemailList.push(currentItem);
            });
            data.listOfCustomEmail.forEach(currentItem => {
                let emailData = {};
                emailData.Subject = currentItem.cmsecureemail__Subject__c;
                emailData.FromName = currentItem.cmsecureemail__FromAddress__c;
                emailData.FromAddress = currentItem.cmsecureemail__FromAddress__c;
                emailData.ToAddress  = currentItem.cmsecureemail__Reply_to__c;
                emailData.TextBody = currentItem.cmsecureemail__HtmlBody__c;
                emailData.Id = currentItem.Id;
                emailData.MessageDate = currentItem.CreatedDate;
                emailData.recepients = currentItem.cmsecureemail__Recipients__c;
                emailData.recordType = (currentItem.RecordType.Name == 'Outbound' ? true : false);
                emailData.isCustomEmail = true;
                tempemailList.push(emailData);
            });
            tempemailList.sort(function(a,b){
                return new Date(b.MessageDate) -  new Date(a.MessageDate)
            });
            this.emailList = tempemailList;

            console.log(tempemailList);
        } else if (error) {
            this.error = error;
        }
    }

    handleonClick(event){
        event.preventDefault();
        this.selectedEmail = undefined;
        //eslint-disable-next-line no-console
        console.log('function',this.selectedEmail);
        const recId = event.target.dataset.currentindex;
        //alert('recId'+recId);
        const inputValue = event.target.value;
        const parentIndex = event.target.name;
        //eslint-disable-next-line no-console
        console.log('function called',recId);
        if(recId){  
            this.selectedEmail=this.emailList.find(email=>email.Id==recId);
            //eslint-disable-next-line no-console
            console.log('function called',this.selectedEmail.Subject);
            if(this.selectedEmail){
                this.isModalOpen =true;
                //eslint-disable-next-line no-console
                console.log('function called',this.isModalOpen);
            }   
        }  
    }

    openModal(){
         this.isModalOpen = true;
         }

    handleReply() {
        this.isREReplyClick = this.selectedEmail.Subject.includes("RE : ") ? true : false;
        this.isReplyClick = true;
    }

    handleForward(){
        this.isForwardClick = true;
    }

    handleRefresh() {
        getEmailList({})
        .then(data => {
            var tempemailList = [];
            data.listofStanderedEmail.forEach(currentItem => {
                tempemailList.push(currentItem);
            });
            data.listOfCustomEmail.forEach(currentItem => {
                let emailData = {};
                emailData.Subject = currentItem.cmsecureemail__Subject__c;
                emailData.FromName = currentItem.cmsecureemail__FromAddress__c;
                emailData.FromAddress = currentItem.cmsecureemail__FromAddress__c;
                emailData.ToAddress  = currentItem.cmsecureemail__Reply_to__c;
                emailData.TextBody = currentItem.cmsecureemail__HtmlBody__c;
                emailData.Id = currentItem.Id;
                emailData.MessageDate = currentItem.CreatedDate;
                                
                emailData.isCustomEmail = true;
                tempemailList.push(emailData);
            });
            tempemailList.sort(function(a,b){
                return new Date(b.MessageDate) -  new Date(a.MessageDate)
            });
            this.emailList = tempemailList;
            
            console.log(tempemailList);
        })
        .catch(error => {
            this.error = error;
        });
    }

    handleTextarea(event) {
        this.textareaReply = event.currentTarget.value;
    }

    handleSendOnReply(event) {
        var emailType = event.target.value;
        alert(emailType);
        alert(this.selectedEmail.Subject);
        alert(this.selectedEmail.FromAddress);
        alert(this.selectedEmail.ToAddress);
        alert(this.textareaReply + '<br/>' + 'From : ' + this.selectedEmail.FromAddress + '<br/>To : ' + this.selectedEmail.ToAddress + '<br/>' + this.selectedEmail.TextBody);
        alert(this.selectedEmail.recepients);
        if(!this.selectedEmail.Subject.includes("RE : ")){this.selectedEmail.Subject = 'RE : ' + this.selectedEmail.Subject}
        updateReplyEmail({
            emailType : emailType,
            subject : this.selectedEmail.Subject,
            formAddress : this.selectedEmail.FromAddress,
            toAddress : this.selectedEmail.ToAddress,
            emailBody : this.textareaReply + '<hr/>' + 'From : ' + this.selectedEmail.FromAddress + '<br/>To : ' + this.selectedEmail.ToAddress + '<br/>Date : '+ this.selectedEmail.MessageDate + '<br/><br/>' + this.selectedEmail.TextBody,
            recepients : this.selectedEmail.cmsecureemail__Recipients__c,
        })
        .then(result => {
            alert(result);
            this.isModalOpen =false;
            this.isReplyClick = false
            this.handleRefresh();
        })
        .catch(error => {
            alert(error);
            console.log(error);
        })
    }

    handleCancelOnReply() {
        this.isReplyClick = false;
    }


    // sortBy(field, reverse, primer){
    //     var key = primer ? function(x) { return primer(x[field]); } : function(x) { return x[field]; };
    //     return function(a, b) {
    //         a = key(a);
    //         b = key(b);
    //         return reverse * ((a > b) - (b > a));
    //     };
    // }

    // sortBy(fieldName, sortDirection) {
    //     let parser = (v) => v;
    //     parser = (v) => (v && new Date(v));
       
    //     let sortMult = sortDirection === 'asc'? 1: -1;
    //     this.data = sortResult.sort((a,b) => {
    //         let a1 = parser(a[fieldName]), b1 = parser(b[fieldName]);
    //         let r1 = a1 < b1, r2 = a1 === b1;
    //         return r2? 0: r1? -sortMult: sortMult;
    //     });
    // }
}