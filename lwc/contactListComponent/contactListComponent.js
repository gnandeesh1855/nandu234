import { LightningElement, wire } from 'lwc';
import { reduceErrors } from 'c/ldsUtils';
import getContactsList from '@salesforce/apex/ContactListController.getContactList';

export default class ContactListComponent extends LightningElement {
    @wire(getContactsList)contacts;

    get errors(){
        return this.contacts.error ? reduceErrors(this.contacts.error) : [];
    }
}