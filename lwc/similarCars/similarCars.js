import { LightningElement,api, wire } from 'lwc';
import getSimilarCars from '@salesforce/apex/carController.getSimilarCars';
import { getRecord } from 'lightning/uiRecordApi';
import MAKE_TYPE from '@salesforce/schema/Car__c.Make__c'
import { NavigationMixin } from 'lightning/navigation';
import CAR_OBJECT from '@salesforce/schema/Car__c';

export default class SimilarCars extends NavigationMixin(LightningElement) {
    @api recordId
    similarCars
    mapres
    @wire(getRecord,{recordId:'$recordId',fields:MAKE_TYPE}) car;

    fetchSimilarCars(){
        getSimilarCars( {
            carId : this.recordId,
            makeType :this.car.data.fields.Make__c.value
        }).then(result=>{
            console.log(result);
            this.similarCars = result;
            console.log(result);
            console.log(JSON.stringify(result));
            let ress = JSON.parse(JSON.stringify(result));
            
            console.log(ress);
            console.log('----');
            ress.forEach(res=>{
                console.log('foreach')
                console.log(res)                
                res.Name = "Tets name";
                res['adding param']= 'Add 1'
                res['Product__r']=res
                console.log(res.Id)
                console.log(res.Name)
            })
            this.mapres = ress.map(res=>{
                console.log('map')
                console.log(res)
                res.Name = "Tets name";
                res['adding param']= 'Add 1'
                res['Product__r']=res;
                console.log(res.Id)
                console.log(res.Name)
                return ress;
            })
            console.log(this.mapres);


        }).catch(error=>{
            console.error(error)
        })
    }

    handleViewDetailsClick(event){
        console.log(event.target.dataset.id)
        console.log(event.target.dataset)
        this[NavigationMixin.Navigate]({
            type:'standard__recordPage',
            attributes:{
                recordId: event.target.dataset.id,
                objectApiName:CAR_OBJECT.objectApiName,
                actionName:'view'
            }
        })
    }
}