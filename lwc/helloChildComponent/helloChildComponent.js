import { LightningElement,api } from 'lwc';
export default class HelloChildComponent extends LightningElement {
    @api message;
    @api cardheading;
    @api isValid;
    @api carouselDetails = [];
    @api progressValue ;

    val = 20;

    @api changeHandler(event){
        this.val = event.target.value;
    }

     resetSlider(event){
        this.val = 50;
    }

}