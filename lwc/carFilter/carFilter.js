import { LightningElement,wire } from 'lwc';
import { getObjectInfo,getPicklistValues } from 'lightning/uiObjectInfoApi';
import CAR_OBJECT from '@salesforce/schema/Car__c';


import CATEGORY_FIELD from '@salesforce/schema/Car__c.Category__c'
import MAKE_FIELD from '@salesforce/schema/Car__c.Make__c'

// import msg channel

import { publish, MessageContext } from 'lightning/messageService';
import CARS_IMPORT_MESG from '@salesforce/messageChannel/CarsFiltered__c';


export default class CarFilter extends LightningElement {
    timer;
    filters = {
        "searchKey" : '',
        "maxprice": 999999
    }
    //load from LMS
    @wire(MessageContext)
    messageContext;


    @wire(getObjectInfo, {objectApiName:CAR_OBJECT}) carObjectInfo;

    @wire(getPicklistValues, 
        {fieldApiName:CATEGORY_FIELD,
            recordTypeId:'$carObjectInfo.data.defaultRecordTypeId'        
        }) categories

        @wire(getPicklistValues, 
        {fieldApiName:MAKE_FIELD,
            recordTypeId:'$carObjectInfo.data.defaultRecordTypeId'        
        }) makeType;
        
    
    handleSearchKeyChange(event){
        console.log(event.target.value);
        this.filters = {...this.filters, "searchKey": event.target.value}
        this.sendDataToCarList()
    }

    handleMaxPriceChange(event){
        console.log(event.target.value);
        this.filters = {...this.filters, "maxprice": event.target.value}
        this.sendDataToCarList()
    }

    handleCategoryChange(event){

        if(!this.filters.categories){
            const categories = this.categories.data.values.map(item=> item.value)
            console.log(categories);
            const makeType = this.makeType.data.values.map(item=> item.value)
            console.log(makeType);
            console.log(this.filters);
            this.filters = {...this.filters,categories,makeType}
            console.log(this.filters);
        }

        const {name,value} = event.target.dataset;
        console.log('name',name);
        console.log(this.filters[name]);
        console.log("value",value);
        if(event.target.checked){
            console.log("---111--");
            if(!this.filters[name].includes(value)){
                this.filters[name] = [...this.filters[name], value]
                console.log(this.filters[name]);
            }
        }else{
            console.log("---222--");
            this.filters[name] =  this.filters[name].filter(item=>item !==value)
            console.log("---333--");
            console.log(this.filters[name]);
        }
        this.sendDataToCarList()

    }

    sendDataToCarList(){
        console.log('sendDataToCarList');
        window.clearTimeout(this.timer)
        this.timer = window.setTimeout(() => {
            publish(this.messageContext, CARS_IMPORT_MESG, {
                filters:this.filters
            })
        },400)
    }
}