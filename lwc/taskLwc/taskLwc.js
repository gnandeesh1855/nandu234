import { LightningElement,track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getTaskList from '@salesforce/apex/taskLwcApex.getTaskList';
import getDuplicateTask from '@salesforce/apex/taskLwcApex.getDuplicateTask';

const columns = [
    { label: 'Task Name', fieldName: 'Name', type: 'text' },
    { label: 'Status', fieldName: 'Status__c', type: 'text' },
    { label: 'Task Type', fieldName: 'Task_Type__c', type: 'text' },
    { label: 'Description', fieldName: 'Description__c', type: 'text' },
    { label: 'Estimated Completion date', fieldName: 'Estimated_Complete_Date__c', type: 'text' },
    { label: 'Created Date', fieldName: 'Created_Date__c', type: 'text' }
]
export default class TaskLwc extends NavigationMixin(LightningElement) {
    // @track loader = false;
    @track error = null;
    @track pageSize = 10;
    @track pageNumber = 1;
    @track totalRecords = 0;
    @track totalPages = 0;
    @track recordEnd = 0;
    @track recordStart = 0;
    @track isPrev = true;
    @track isNext = true;
    @track tasks = [];
    @track name;
    @track generatedurl;
    @track columns=columns;
    @track description;
    @track tasktype;
    @track onsuccess=false;
    @track iserror=false;
    @track completeddate='';
    @track isdateerror='';
    @track dateerrorstring='';
    @track alltasks='brand';
    @track progress = 'neutral';
    @track completed = 'neutral';
    @track Status='';
    @track InProgressRecords =0;
    onhandleClick(event){
        event.preventDefault();
        if(!this.iserror && ! this.isdateerror){
            const fields = event.detail.fields;
             
              this.template.querySelector('lightning-record-edit-form').submit(fields);
              

        }
        else{
           
            const event =new ShowToastEvent({
               
                type:'error',
                variant: 'error',
                message: 'Error Occured when trying to create Task',
               
            })
            this.dispatchEvent(event);
        
            
        }
        
    }

    get gettotalRecords(){
        
        if(this.Status==''){
           
            return this.totalRecords;
        }
        else if(this.Status=='In Progress'){
           
            return this.InProgressRecords;
        }
        else if(this.Status=='Completed'){
            
            return this.totalRecords-this.InProgressRecords;
        }
        else{
            
            return 0;
        }
        
    }
    onhandleChange(event){
        var name = event.target.name;
        if(name=='name'){
            this.name=event.target.value;
            
           
        }
        else if(name=='completeddate'){
            this.completeddate=event.target.value;
            var date =new Date();
            
            console.log(date);
           // console.log(Math.abs(new Date()/(1000*60*60*24)));
            var days=Math.ceil(( new Date(this.completeddate) - new Date() ) /(1000*60*60*24));

            console.log(days);
            

        console.log(date < new Date(this.completeddate));
            if(days > 30){
                this.isdateerror=true;
                this.dateerrorstring ='Estimated Completion Date should not be greater than 30 days';
            }
            else if(days ==  0){
                this.isdateerror=true;
                this.dateerrorstring = 'Estimated Completion should not be less than one day';
            }
            else if(days <0){
                this.isdateerror=true;
                this.dateerrorstring = 'Estimated Completion date should not be previous date';
            }
            else if(days >  0 && days <= 30){
                this.isdateerror=false;
                this.dateerrorstring='';

            }

        }
        else if(name =='alltasks'){
            this.Status='';
            this.gettaskdetails();
            this.progress='neutral';
            this.completed='neutral';
            this.alltasks='brand';

        }
        else if(name =='progress'){
            this.Status = 'In Progress';
           
            this.progress='brand';
            this.completed='neutral';
            this.alltasks='neutral';
            this.pageNumber = 1;
            console.log(this.totalPages , this.totalRecords , this.recordEnd,this.recordStart ,this.Status);
    
    this. totalPages = 0;
    this. recordEnd = 0;
    this. recordStart = 0;
    console.log(this.totalPages , this.totalRecords , this.recordEnd,this.recordStart,this.Status);
    this.gettaskdetails();

        }
        else if(name =='completed'){
            this.Status ='Completed';
            this.pageNumber = 1;
            console.log(this.totalPages , this.totalRecords , this.recordEnd,this.recordStart,this.Status);
    this. totalPages = 0;
    this. recordEnd = 0;
    this. recordStart = 0;
    console.log(this.totalPages , this.totalRecords , this.recordEnd,this.recordStart,this.Status);
            
            this.progress='neutral';
            this.completed='brand';
            this.alltasks='neutral';
            this.gettaskdetails();

        }

        
        
    }

    onFocusOutEvent(event){
        if(event.target.name=='name'){
            console.log('name onblur');
            getDuplicateTask({value:this.name})
            .then(result => {
               
                if(result){
                    
                    this.iserror=true;
                    
    
                }
                else{
                    
                    this.iserror=false;
                }
    
            })
            .catch(error => {
                console.log(error);
            });
           
        }
       
       
       

    }

    connectedCallback() {
        this.gettaskdetails();
        // .then((result)=>{
        //     console.log('then in connected callback');
        // })
    }
 
    //handle next
    handleNext(){
        this.pageNumber = this.pageNumber+1;
        this.gettaskdetails();
    }
 
    //handle prev
    handlePrev(){
        this.pageNumber = this.pageNumber-1;
        this.gettaskdetails();
    }
    get istableshown(){
        return this.tasks.length >0 ? true :false;
    }

    gettaskdetails(){
        // this.loader = true;
        console.log('before getatsks' ,this.totalRecords,this.Status);
        getTaskList({pageSize: this.pageSize, pageNumber : this.pageNumber ,Status :this.Status})
        .then(result => {
           
            this.loader = false;
            if(result){
                console.log('This.stats',this.Status ,this.totalRecords);
                var resultData = JSON.parse(result);
                this.InProgressRecords =resultData.InProgressRecords;
                //console.log('in get resuts '+ resultData.InProgressRecords);
                this.tasks = resultData.tasks;
                console.log('in get resuts '+ resultData.tasks);
                this.pageNumber = resultData.pageNumber;
                this.totalRecords = resultData.totalRecords;
                 console.log(resultData.tasks.length);
                this.recordStart = resultData.recordStart;
                this.recordEnd = resultData.recordEnd;
               var totalrecords=0;
                if(this.Status=='In Progress'){
                    console.log('inproges staus');
                    totalrecords = resultData.InProgressRecords;
                    
                }
                else if(this.Status=='Completed'){
                    console.log('compeletd styays');
                    totalrecords = (resultData.totalRecords- resultData.InProgressRecords);
                    
                }
                else{
                    console.log('else in statys');
                    totalrecords =resultData.totalRecords;
                }
                
                console.log('totalrecords',totalrecords);
                this.totalPages = Math.ceil(totalrecords / this.pageSize);
                    this.isNext = (this.pageNumber == this.totalPages || this.totalPages == 0);
                    this.isPrev = (this.pageNumber == 1 || totalrecords < this.pageSize);
                
               
              
            }
        })
        .catch(error => {
            this.loader = false;
            this.error = error;
        });
    }



   get alltasklabel(){
       return 'ALL Tasks ('+this.totalRecords+')';
   }

    get getInProgresslabel() {
        var count=this.InProgressRecords;
        console.log(count + 'get in');
        if(this.InProgressRecords){
            return  'In Progress (' + count  + ')';
        }
        return 'In Progress (0)'
           
    
       
    }

    get getcompletedlabel(){
        var count = this.totalRecords - this.InProgressRecords;
        return 'Completed (' + count  +')';
    }

    

    handleSuccess(event){
       
       this.loader=true;

       if(!this.iserror && ! this.isdateerror){
       
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.detail.id,
                actionName: 'view',
            },
        }).then((url) => {
            this.generatedurl=url;
            //alert(generatedurl);
            const event =new ShowToastEvent({
                title: 'Success!',
                type:'success',
                variant: 'success',
                message: 'Task {1} was created!',
                messageData: [
                    this.name,
                    {
                        url:this.generatedurl,
                        label: this.name,
                    },
                ],
            })
          // window.location.reload();
          
          //this.gettaskdetails();
        
        this.dispatchEvent(event);
        this.tasktype='';
        this.name='';
        this.description='';
        this.completeddate='';
      
           
           
        })
        .then((result)=>{
           
            this.gettaskdetails();
        })
        .catch((error)=>{
            console.log('error');
            console.log(error);
        })

      
    }
    
   

       
      
       
    }
    
}