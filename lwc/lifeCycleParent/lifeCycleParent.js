import { LightningElement } from 'lwc';
export default class LifeCycleParent extends LightningElement {
    name;
    nandu
    isShowVisble = false;
    constructor(){
        super()
        console.log('Parent Constructor');
    }
    connectedCallback() {
        this.nandu= 'nan';
        console.log('Parent Connected callback');
        this.name = 'nanu';
        console.log('Parent Connected callback',this.nandu);
    }
    renderedCallback(){
        console.log('Parent rendered callback');
    }
    handleClick(){
        console.log('handleclick');
        this.isShowVisble = !this.isShowVisble;
    }

}