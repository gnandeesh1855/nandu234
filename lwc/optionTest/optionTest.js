import { LightningElement,track ,api,wire} from 'lwc';
import getContactList from '@salesforce/apex/AccountsController.getContactList';
import { getRecord } from 'lightning/uiRecordApi';
import ContactName from '@salesforce/schema/Contact.Name';
const namefields=[ContactName];

export default class OptionTest extends LightningElement {
    @track contacts;
    @track error;
    @api searchKey='';
    @api recordId;
    @api record1;
   
    handleLoad() {
        /* eslint-disable no-console */
        console.log('Search Value ' +this.searchKey);
     
        getContactList({ searchvalue: this.searchKey })
            .then(result => {
                this.contacts = result;
            })
            .catch(error => {
                this.error = error;
            });
    }
   

    @wire(getRecord, { recordId: '$recordId', fields: ['Contact.Name'] })
    record;
   
    handleChange(event){
        /* eslint-disable no-console */
        console.log(this.record1);
        this.searchKey=event.target.value;
    }
}