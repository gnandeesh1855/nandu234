import { LightningElement ,api} from 'lwc';

export default class ParentComponent extends LightningElement {
    @api input='mouni';
    @api showChildSlots='false';
  
    get inputMethod(){
        return `${this.input}`.toUpperCase();
    }
    handleChange(event){
        this.input=event.target.value;
    }
    increment(){
        this.template.querySelector('c-lwc-child').increment();
    }
    slottoggle(){
        this.showChildSlots= !this.showChildSlots;
    }
  
}