import { LightningElement } from 'lwc';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import NAME_FIELD from '@salesforce/schema/Contact.FirstName';
import LASTNAME from '@salesforce/schema/Contact.LastName';
import BIRTHDATE from '@salesforce/schema/Contact.Birthdate';
import PHONE from '@salesforce/schema/Contact.Phone';
import CART from '@salesforce/schema/Contact.Cart__c';
export default class RecordEditForm extends LightningElement {

    showcustomValidationForm=false;
    objectname = CONTACT_OBJECT;
    fields = {
        name:NAME_FIELD,
        lastname:LASTNAME,
        birthdate:BIRTHDATE,
        phone:PHONE,
        cart:CART
    }
    onSubmitHanlder(event){

        console.log('onSubmitHanlder',event.detail);
        console.log('onSubmitHanlder',event.detail.field);
        console.log('onSubmitHanlder',event.detail.fields);
        event.preventDefault();
        const fields = event.detail.fields;
        console.log(JSON.stringify(fields));
        fields.Phone = '123444';
        console.log('onSubmitHanlder',JSON.stringify(fields) );
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }
    OnSucessHadler(event){
        console.log('OnSucessHadler',event.detail);
        const payload = event.detail;
        console.log(JSON.stringify(payload));
    }
    saveHandler(event){
        this.fields.name = 'My New Value';
    }

    onReset(event){
        let resetvalues = this.template.querySelectorAll('lightning-input-field');
        resetvalues.forEach(fieldValues =>{fieldValues.reset()});
    }
    onResetName(event){
        let resetname = this.template.querySelectorAll('.conName');
        Array.from(resetname).forEach(resetnam=>{resetnam.reset()});
    }

    showCustomFOrm(){
        this.showcustomValidationForm = true;
    }

    // Custom Edit code
    lastname ;
    handleInput(event){
        this.lastname = event.target.value;
    }

    
    onCustomSubmitHanlder(event){
        event.preventDefault()
        let inputCmp = this.template.querySelector('.inputvalue');
        const fields = event.detail.fields;
        
        let lastnamevalue = this.lastname;
        console.log('onCustomSubmitHanlder',JSON.stringify(fields));
        if(!lastnamevalue.includes('nandu')){
            inputCmp.setCustomValidity("Enter valid name");
        }else{
            fields.LastName = this.lastname;

            this.template.querySelector('.customrecordedit').submit(fields);
        }
        inputCmp.reportValidity();
    }

    OnCustomSucessHadler(){

    }
}