trigger DemotriggerOppty on Opportunity (before update, before insert, before delete) {
    
    /* Opportunity oppty= [select id,Account.Rating from Opportunity where id = :Trigger.New limit 1];
    
    // :Trigger.New[0].id
 //   Account acc = [select id,Rating from Account where id = :Trigger.New[0].accountid limit 1];
    for(Opportunity opp: Trigger.New){
        if(oppty.Account.Rating == null){
            opp.addError('You cannot update this oppty because respective account rationg is null');
        }
    } */
    
    if(Trigger.isBefore && Trigger.isInsert){
        set<id> accountids = new set<id>();
        for(Opportunity oo: Trigger.new){ 
          //  accountids.add(oo.id);
            accountids.add(oo.accountid);
        }
     //   system.debug('----'+ opptyids);
        Account acc = [select id,Description from Account where id in :accountids]; 
    
        for(Opportunity opp: Trigger.new){
            opp.Description = acc.Description;
        }
    }
    
    if(Trigger.isBefore && Trigger.isDelete){
        for(Opportunity opp: Trigger.old){
            if(opp.AccountId != null){
                opp.addError('You cannot delete this oppty');   
            }
        }
    }
    
}