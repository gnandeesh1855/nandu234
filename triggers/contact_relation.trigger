//Create the field called “Contact Relationship” checkbox on the Contact Object and Create the related object called “Contact Relationship”
// which is related list to the Contacts.(Look Up Relationship).

// Now logic is when we create contact by checking Contact Relationship checkbox, 
// then Contact Relationship will be created automatically for that contact.

trigger contact_relation on Contact (after insert,after update,before delete) {
   List<Contact_Relationship__c> newCR = new List<Contact_Relationship__c>();
    if (Trigger.isafter && Trigger.isupdate || Trigger.isafter && Trigger.isinsert){ 
    for( Contact con: Trigger.new){
        if(con.Contact_Relation__c== true){
           Contact_Relationship__c CR = new Contact_Relationship__c();
            CR.Contact_Relationship__c = con.Id;
            CR.Name = con.Name;
            newCR.add(CR);
        }
        }
    }
    insert newCR;
  /*  if(Trigger.isbefore && Trigger.isdelete){
  //   List<Contact_Relationship__c> deleteCR = new List<Contact_Relationship__c>();   
        for(Contact c: Trigger.old){
            
            
      //      if(c.Contact_Relation__c == false){
                  Contact_Relationship__c CR = new Contact_Relationship__c();
           List<Contact_Relationship__c> CR1 = 'Select id from Contact_Relationship__c where Contact in :GlobalUtility.getuniqueids(trigger.old)';
               // CR.Contact_Relationship__c = c.Id;
            for(Contact_Relationship__c CC : CR){
                 delete CC;
            }}
       // }
      // delete deleteCR;
    }*/
}