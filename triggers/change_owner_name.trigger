//When we change the Owner of the Contact Relationship, 
//then the Owner name will be automatically populated in the Contact Relationship Name field.

trigger change_owner_name on Contact_Relationship__c (before update) {
	//Set<id> owner_name = new Set<id>();
    for(Contact_Relationship__c CR : Trigger.new){
        //owner_name.add(CR.ownerId);
        if( CR.OwnerId != Trigger.oldmap.get(CR.Id).OwnerId){
            
          System.debug('change_owner_name'+'  '+Trigger.oldmap.get(CR.id).OwnerId);
            CR.Name = CR.Owner.Username;           
            
        }
    
    }
   // List<User>  user= [Select Name,Id from User where id in :owner_name];
}