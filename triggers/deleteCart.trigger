trigger deleteCart on eBooks_Cart_Items__c (after delete) {
    if(Trigger.isDelete && Trigger.isAfter){
        System.debug(Trigger.Old[0].Cart_Name__c);
        List<eBooks_Cart__c> cart =[Select Id, (select Id from eBooks_Cart_Items__r) from eBooks_Cart__c where Id=:Trigger.Old[0].Cart_Name__c limit 1];
       System.debug('Cart Details -----> '+ cart);
        if(cart[0].eBooks_Cart_Items__r.size()==0){
            delete cart;
        }
    }


}