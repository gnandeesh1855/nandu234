//Create “Sales Rep” field with data type (Text) on the Account Object. When we create the Account record, 
//the Account Owner will be automatically added to Sales Rep field. When we update the Account owner of the record, 


trigger update_salerep_on_accowner on Account (before insert,before update) {
    
    Set<id> accownerid = new Set<id>();
    for (Account acc: Trigger.new){
        accownerid.add(acc.Ownerid);
    }
  //Id acc= [Select Name FROM User where id in:accownerid];
    // Map<Id,User> userid = new map<Id,User>();
     List<user> user_name = [Select Name FROM User where id in:accownerid];
    
         for (Account acc: Trigger.new){
             for (user u : user_name){
      
      //  User use = userid.get(acc.Ownerid);
    //  userid.put(acc.ownerid,user_name);
    
       acc.name_on_salesrep__c = u.Name;
            }
    }
}