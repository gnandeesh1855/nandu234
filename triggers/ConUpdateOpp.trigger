trigger ConUpdateOpp on Contact (before insert , before update) {
    if(Trigger.isBefore){
        if(Trigger.isupdate){
            system.debug('account Id --> '+ Trigger.new[0].AccountId);
            List<Opportunity> accopp=[select id ,AccountId,Name ,StageName from Opportunity where AccountId = :Trigger.new[0].AccountId];
            for(Contact c : Trigger.new){
                for(Opportunity opp:accopp){
                if(opp.StageName=='Prospecting'){
                    c.adderror('cannot update contacts as one of the opportunity has stage prospecting');
                }
            }
            }
        }
    }

}