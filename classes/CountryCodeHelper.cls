public class CountryCodeHelper {
	public static String getCountryCode(String country) {
    Country_Code__mdt countryCode = [
      SELECT Id, MasterLabel, Country_Code__c
      FROM Country_Code__mdt
      WHERE MasterLabel = :country
      LIMIT 1
    ];
  	Map<String, Country_Code__mdt> mapFileTypes = Country_Code__mdt.getAll();
        Country_Code__mdt txtFileTypeDetails = Country_Code__mdt.getInstance('canada');
//System.debug(‘fileTypes::’+mapFileTypes);
    return countryCode.Country_Code__c;
  }
}