public class returnRecordsClass {
    @Auraenabled
    public static List<opportunity> getopttys(String namesearch){
         List<opportunity> opp = new  List<opportunity>();
        String searchstring = String.isBlank(namesearch) ? '%%' : namesearch+'%';
        system.debug('---'+searchstring);
        opp = [Select id,name,account.Name,closeDate,amount from Opportunity where name LIKE :searchstring limit 20 ];  
        System.debug('---'+opp);
        return opp;
    }

}