public class eBookApex {
    @AuraEnabled(cacheable=true)
    public static List<E_Books__c> getEbooks(String value){
    return [Select Id,Description__c,Price__c, Name ,Book_Id__c from E_Books__c where Name like : '%%'+value+'%%'];
     }
     @AuraEnabled(cacheable=true)
    public static List<eBooks_Cart_Items__c> getCartDetails(){
       return [select Id ,Quantity__c,Book_Name__c, Book_Name__r.Name,Total_Price__c ,Book_Name__r.Price__c,Cart_Name__r.OwnerId from eBooks_Cart_Items__c where Cart_Name__r.OwnerId =: UserInfo.getUserId() ];
        
    }
    @AuraEnabled(cacheable=true)
    public static List<eBooks_Cart_Items__c> getCartDetails1(){
       return [select Id ,Quantity__c,Book_Name__c, Book_Name__r.Name,Total_Price__c ,Book_Name__r.Price__c,Cart_Name__r.OwnerId from eBooks_Cart_Items__c where Cart_Name__r.OwnerId =: UserInfo.getUserId() ];
        
    }

    public class eBookDetailsWrapper{
        @AuraEnabled
        public List<eBooks_Cart_Items__c> cartItemsList{get;set;}
        @AuraEnabled
        public List<E_Books__c> ebooksList{get;set;}
      
    }
   
    @AuraEnabled(cacheable=true)
    public static eBookDetailsWrapper getEbooksList(String value){
        eBookDetailsWrapper Details = new eBookDetailsWrapper();
        Details.ebooksList = getEbooks(value);
        Details.cartItemsList =getCartDetails();
   
        return Details;
    }
    
     @AuraEnabled(cacheable=false)
    public static Id getCartId(){
        List<eBooks_Cart__c> us= [select Id  from eBooks_Cart__c where OwnerId =: UserInfo.getUserId() limit 1];
        if(us.size()>0){
            return us[0].Id;
        }
        else{
            eBooks_Cart__c cart = new eBooks_Cart__c();
            insert cart;
            return cart.Id;
        }
    }
     @AuraEnabled(cacheable=false)
    public static Boolean updateCartItems(eBooks_Cart_Items__c cartItem,Integer Quantity){
          List<eBooks_Cart_Items__c> tobeupdated = new List<eBooks_Cart_Items__c>();
            eBooks_Cart_Items__c item = new eBooks_Cart_Items__c(Id = cartItem.Id , Quantity__c= Quantity ,Total_Price__c = Quantity *(cartItem.Book_Name__r.Price__c));
            tobeupdated.add(item);
        if(Quantity>0){
            if(tobeupdated.size()>0){
                update tobeupdated;
                return true;
            }
       
        }
        else if(Quantity<=0){
            Boolean value=deleteCartItems(cartItem);
            return value;
        }
        return false;
            
    }
     @AuraEnabled(cacheable=true)
    public static Boolean deleteCartItems(eBooks_Cart_Items__c cartItem){
         if(cartItem!=null){
                delete cartItem;
                return true;
            }
        return false;
    }
    
    @AuraEnabled(cacheable=false)
    public static Id addToCart(E_Books__c eBook){
        String cartId = getCartId();
        List<eBooks_Cart_Items__c> cartItem = [Select Id ,Quantity__c,Book_Name__c,Total_Price__c from eBooks_Cart_Items__c where Cart_Name__c =:cartId and Book_Name__c =:eBook.Id limit 1 ];
        if(cartItem.size()>0){
             List<eBooks_Cart_Items__c> tobeupdated = new List<eBooks_Cart_Items__c>();
            eBooks_Cart_Items__c item = new eBooks_Cart_Items__c(Id = cartItem[0].Id , Quantity__c= cartItem[0].Quantity__c+1 ,Total_Price__c = (cartItem[0].Quantity__c +1) * eBook.Price__c);
            tobeupdated.add(item);
            if(tobeupdated.size()>0){
                update tobeupdated;
            }
        }
        else{
            eBooks_Cart_Items__c Item = new eBooks_Cart_Items__c(Quantity__c=1,Total_Price__c = eBook.Price__c ,Book_Name__c =eBook.Id ,Cart_Name__c=cartId);
                    insert Item;
                    
        }
        return cartId;
        
        
               
         
        
    }

}