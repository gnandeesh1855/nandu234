public class AccountContacts {
    @AuraEnabled
    public static List<Contact> getContactsList(String sortField, String isAsc,string searchVal){
        String SOQL='Select FirstName,LastName,Name,Phone,Email From Contact ';
        string s=string.isBlank(searchVal)?'%%':'%%'+searchVal+'%%';
        SOQL+=' where Name like  \'%'+searchVal+'%\'';
        system.debug(SOQL);
        
        
        
        if(sortField!=null){
            system.debug(sortField);
            SOQL+=' order by '+sortField;
        }
        if(isAsc!=null){
            system.debug(isAsc);
            SOQL+=' '+isAsc;
        }
        SOQL+=' LIMIT 10';
        
        system.debug(SOQL);
        list <Contact> lstResult;
        lstResult=database.query(SOQL);
        return lstResult;
        
    }
    
    @AuraEnabled
    public static boolean updateContacts(List<Contact> contacts,Id accId){
        for(Contact con:contacts){
            con.AccountId=accId;
        }
        if(contacts.size()>0){
            update contacts;
            return true;
        }
        else{
            return false;
        }
        
    }
}