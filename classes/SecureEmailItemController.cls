public with sharing class SecureEmailItemController {
   
        @AuraEnabled(cacheable=true)
        public static EmailWrapper getEmails() {
            EmailWrapper ew = new EmailWrapper();
            List<OrgWideEmailAddress> lstEmail = [select id, Address, DisplayName from OrgWideEmailAddress];
            ew.listofOrgWideEmails = lstEmail;
            ew.currentUserName = UserInfo.getName();
            ew.currentUserEmail = UserInfo.getUserEmail();
            return ew;
        }

        public class EmailWrapper{
            @AuraEnabled public list<OrgWideEmailAddress> listofOrgWideEmails;
            //@AuraEnabled public list<cmsecureemail__Secure_EmailMessage__c> listOfCustomEmail;
            @AuraEnabled public String currentUserName;
            @AuraEnabled public String currentUserEmail;

        }

        @AuraEnabled()
        public static cmsecureemail__Secure_EmailMessage__c sendMail(String Subject,String FromName,String FromAddress,String ToAddress,String Contact,String HtmlBody,String TextBody,String Id,Datetime MessageDate,String recepients,Boolean recordType) {
            cmsecureemail__Secure_EmailMessage__c email = new cmsecureemail__Secure_EmailMessage__c();
            
            
            email.cmsecureemail__FromAddress__c = FromAddress;
            email.cmsecureemail__Subject__c = Subject;
            //email.From_Name__c = FromName;
            email.cmsecureemail__Reply_to__c = ToAddress;
            email.cmsecureemail__Contact__c = Contact;
            email.cmsecureemail__HtmlBody__c = HtmlBody;
email.cmsecureemail__TextBody__c = TextBody;
            //email.Id = Id;
            //email.CreatedDate = MessageDate;
            email.cmsecureemail__Recipients__c = recepients;
            if(recordType == true){
            Id recordTypeId = Schema.SObjectType.cmsecureemail__Secure_EmailMessage__c.getRecordTypeInfosByName().get('Outbound').getRecordTypeId();
            email.RecordTypeId  = recordTypeId; 
            }
            insert email;
            return email;
        }

        @AuraEnabled()
        public static cmsecureemail__Secure_EmailMessage__c sendReplyAllMail(String Subject,String FromName,String FromAddress,String ToAddress,String Contact,String HtmlBody,String TextBody,String bccRecepients,String Id,Datetime MessageDate,String recepients,Boolean recordType) {
            cmsecureemail__Secure_EmailMessage__c email = new cmsecureemail__Secure_EmailMessage__c();
            email.cmsecureemail__FromAddress__c = FromAddress;
            email.cmsecureemail__Subject__c = Subject;
            //email.From_Name__c = FromName;
            email.cmsecureemail__Reply_to__c = ToAddress;
            email.cmsecureemail__Contact__c = Contact;
            email.cmsecureemail__HtmlBody__c = HtmlBody;
            email.cmsecureemail__TextBody__c = TextBody;
            email.cmsecureemail__BccRecipients__c = bccRecepients;
            //email.Id = Id;
            //email.CreatedDate = MessageDate;
            email.cmsecureemail__Recipients__c = recepients;
            if(recordType == true){
            Id recordTypeId = Schema.SObjectType.cmsecureemail__Secure_EmailMessage__c.getRecordTypeInfosByName().get('Outbound').getRecordTypeId();
            email.RecordTypeId  = recordTypeId;
            }
            insert email;
            return email;
        } 
        
    }