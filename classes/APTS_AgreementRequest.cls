public class APTS_AgreementRequest {
    
    public string contractheaderId;
    public List<CONTRACT_HEADER_IN> CONTRACT_HEADER_IN;
    public List<CONTRACT_ITEMS_IN> CONTRACT_ITEMS_IN;
    public class CONTRACT_HEADER_IN {
        public String DOC_TYPE;
        public String SALES_ORG;
       
    }
     public class CONTRACT_ITEMS_IN {
        public String HG_LV_ITEM;
        public String ITM_NUMBER;
        public String PO_ITM_NO;
        
    }
    
}