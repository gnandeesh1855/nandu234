public class SecureEmailInboxController {
	
    @AuraEnabled(cacheable=true)
    public static EmailWrapper getRecordSpecificEmailList(String recordId) {
    	return SecureEmailInboxController.queryAllEmails(recordId);        
    }
    
    @AuraEnabled(cacheable=true)
    public static EmailWrapper getEmailList() {
    	return SecureEmailInboxController.queryAllEmails(null);    
    }
    
    private static EmailWrapper queryAllEmails(String recordId){
        EmailWrapper ew = new EmailWrapper();
        List<EmailMessage> listofStanderedEmail = [
            SELECT 
            	Id, Subject, FromAddress, ToAddress, CreatedDate, FromName, TextBody, MessageDate  
            FROM 
            	EmailMessage WHERE FromName != null
        ];
        
        List<cmsecureemail__Secure_EmailMessage__c> listOfCustomEmail = new List<cmsecureemail__Secure_EmailMessage__c>();
        if(recordId != null && recordId != ''){
        	listOfCustomEmail = [
                SELECT 
                    Id, cmsecureemail__Subject__c, From_Name__c, RecordType.Name, cmsecureemail__Reply_to__c, cmsecureemail__Recipients__c, 
                    cmsecureemail__FromAddress__c, cmsecureemail__BccRecipients__c,  cmsecureemail__Contact__c, cmsecureemail__HtmlBody__c, cmsecureemail__TextBody__c, CreatedDate 
                FROM 
                    cmsecureemail__Secure_EmailMessage__c 
                WHERE
                	 cmsecureemail__Parent_Record_Id__c = :recordId
                ORDER BY
                	CreatedDate ASC
            ];    
        }
        else {
            listOfCustomEmail = [
                SELECT 
                    Id, cmsecureemail__Subject__c, From_Name__c, RecordType.Name, cmsecureemail__Reply_to__c, cmsecureemail__Recipients__c, 
                    cmsecureemail__FromAddress__c, cmsecureemail__BccRecipients__c, cmsecureemail__Contact__c, cmsecureemail__HtmlBody__c, cmsecureemail__TextBody__c, CreatedDate 
                FROM 
                    cmsecureemail__Secure_EmailMessage__c
                ORDER BY
                	CreatedDate ASC
            ];
        }
        ew.listofStanderedEmail = listofStanderedEmail;
        ew.listOfCustomEmail = listOfCustomEmail;
        return ew;
    }
    
    public class EmailWrapper{
        @AuraEnabled public list<EmailMessage> listofStanderedEmail;
        @AuraEnabled public list<cmsecureemail__Secure_EmailMessage__c> listOfCustomEmail;
    }
}