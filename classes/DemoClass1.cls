public class DemoClass1 {
	//public Opportunity NokiaOpportunity;
	
    public void opptymethod(){
        Opportunity NokiaOpportunity1 = [select id,name,OrderNumber__c from Opportunity where id = '0066F000016s2XN'];
        NokiaOpportunity1.OrderNumber__c = '1234';
        update NokiaOpportunity1;
    }
    
    public void createOpptunity(){
        Opportunity opp = new Opportunity();
        opp.name = 'Demo Oportunity';
        opp.CloseDate = System.today();
        opp.StageName = 'prospecting';
        insert opp;
    }
    
    public void deleteOppty(){
        Opportunity deleteopptyrecord = [select id,name,OrderNumber__c from Opportunity where id = '0066F000019nZxX'];
		delete deleteopptyrecord;
    }
}