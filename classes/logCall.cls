public class logCall {
    @AuraEnabled
    public static Id insertTask(){
        
        
         Task newTask = new Task(Description = 'Survey Transaction',
                                        Priority = 'Normal', 
                                        Status = 'Completed',
                                        Subject = 'Checking1',  // setting this makes this task registered as a call
                                        IsReminderSet = true, 
                                        TaskSubtype = 'Call',
                                        WhatId = '0016F000041aqE8QAI',
                                        ReminderDateTime = System.now()+1 
                                           );             
                insert newTask;
        return newTask.Id;
    }

}