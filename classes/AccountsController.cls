public with sharing class AccountsController {
    
    @AuraEnabled(cacheable=true)
    public static List<sObject> getAccounts(String searchvalue){
        system.debug(searchvalue);
       String key=string.isBlank(searchvalue)?'%%': '%'+searchvalue+'%';
       
        string q2='Select Id,Name,Industry,Phone from Account where Name like : key';
        return Database.query(q2);
        
        
    }
    @AuraEnabled(cacheable=true)
    public static List<sObject> getAccounts1(String searchvalue){
        string key=string.isBlank(searchvalue)?'%%': '%'+searchvalue+'%';
       //String key='%'+searchvalue+'%';
       String q='Select Id,Name,Industry,Phone from Account where Name like : key';
        return Database.query(q);
        
    }
    @AuraEnabled(cacheable=true)
    public static List<Contact> getContactList(String searchvalue) {
        string key=string.isBlank(searchvalue)?'%%': '%'+searchvalue+'%';
        string q1='SELECT Id, Name, Title, Phone, Email FROM Contact where Name like :key limit 10';
        
        return Database.query(q1);
        
    }
}