public with sharing class taskLwcApex {
    public taskLwcApex() {

    }



    @AuraEnabled()
    public static Boolean getDuplicateTask(String value){
        List<Task__c> task =[Select Id , Name from Task__c where Name = : value];
        if(task.size()>0)
            return true;
        return false;    
       



    }

    @AuraEnabled()
    public static String getTaskList(Integer pageSize, Integer pageNumber, String Status){
        String jsonDT = '';
        Integer InProgressRecords = [SELECT COUNT() FROM Task__c where Status__c= 'In Progress'];
        System.debug(InProgressRecords);
        Integer totalRecords = [SELECT COUNT() FROM Task__c];
        Integer recordEnd = pageSize * pageNumber;
        Integer offset = (pageNumber - 1) * pageSize;
        AccountDTWrapper objDT =  new AccountDTWrapper();  
        if(Status == ''){
            System.debug('if status');
            objDT.recordEnd = totalRecords >= recordEnd ? recordEnd : totalRecords;
           
        }
        else if(Status == 'In Progress'){
            System.debug('in progress status');
            objDT.recordEnd = InProgressRecords >= recordEnd ? recordEnd : InProgressRecords;
        }
        else if(Status == 'Completed'){
            System.debug('iCompleted');
            objDT.recordEnd = totalRecords - InProgressRecords >= recordEnd ? recordEnd : totalRecords - InProgressRecords;
        }
        //Offset for SOQL
       

        
        
         
        
        objDT.pageSize = pageSize;
        objDT.pageNumber = pageNumber;
        objDT.recordStart = offset + 1;
       
        objDT.totalRecords = totalRecords;
        objDT.InProgressRecords=InProgressRecords;
        if(Status == ''){

        System.debug('if'+ Status);
        objDT.tasks = [select  Id, Name, CreatedById, Created_Date__c, CreatedDate,Estimated_Complete_Date__c,
                            Status__c, Task_Type__c, Description__c from Task__c order by CreatedDate desc
                                 LIMIT :pageSize OFFSET :offset];
        }
        else{
            System.debug('else'+ Status);
            objDT.tasks = [select  Id, Name, CreatedById, Created_Date__c, CreatedDate,Estimated_Complete_Date__c,
            Status__c, Task_Type__c, Description__c from Task__c where Status__c =:Status order by CreatedDate desc
                 LIMIT :pageSize OFFSET :offset];
        }
        jsonDT = JSON.serialize(objDT);
        return jsonDT;
    }

    public class AccountDTWrapper {
        public Integer pageSize {get;set;}
        public Integer pageNumber {get;set;}
        public Integer totalRecords {get;set;}
        public Integer InProgressRecords {get;set;}
        public Integer recordStart {get;set;}
        public Integer recordEnd {get;set;}
        public List<Task__c> tasks {get;set;}
    }
}