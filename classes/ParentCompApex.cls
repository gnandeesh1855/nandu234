public class ParentCompApex {
    @AuraEnabled
    public static List<Account> getAccounts(string searchVal){
        return [select id,Name, AccountNumber, AccountSource from account where Name Like : '%%'+searchVal+'%%'];
    }

}