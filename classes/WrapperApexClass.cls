public class WrapperApexClass{
    @AuraEnabled
    public static List<Contact> getContactsList(String sortField, String isAsc,string searchVal,string AccountId){
        String SOQL='Select Id,FirstName,LastName,Name From Contact ';
        string s=string.isBlank(searchVal)?'%%':'%%'+searchVal+'%%';
        SOQL+=' where Name like  \'%'+searchVal+'%\'';
          system.debug(SOQL);
        if(AccountId!=null){
            SOQL+=' and Account.Id = \'' +AccountId+'\'';
        }
         
          system.debug(SOQL);
        if(sortField!=null){
            system.debug(sortField);
            SOQL+=' order by '+sortField;
        }
        if(isAsc!=null){
            system.debug(isAsc);
              SOQL+=' '+isAsc;
        }
      SOQL+=' LIMIT 10';
        
        system.debug(SOQL);
        list <Contact> lstResult;
      lstResult=database.query(SOQL);
        return lstResult;
      
    }
   
    @AuraEnabled
    public static List < Account > fetchAccts() {
        return [ SELECT Name, Industry FROM Account LIMIT 10 ];
    
   
}
    @AuraEnabled
       public static Contact fetchContacts(Contact c) {
        insert c;   
        return c;    
   
}
    @AuraEnabled
    public static String getBaseUrl(){
        return URL.getSalesforceBaseUrl().toExternalForm();
    }
      
  
    
}