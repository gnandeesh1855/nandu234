public class BeerCart {
    @AuraEnabled
    public static String getCartId(){
        List<Cart__c> Cartlist=[select Cart_Id__c,Name from Cart__c  where User__c=:UserInfo.getUserId()];
        if(Cartlist!=null  && Cartlist.size()>0){
            return Cartlist[0].Id;
        }
        else{
           Cart__c cart=new Cart__c(User__c=UserInfo.getUserId(),Cart_Id__c=String.valueOf(Math.random()));
            insert cart;
            return cart.Id;//test
        }
        
    }
    @AuraEnabled
    public static String createCartItems(List<String> beerList){
        String cartId=getCartId();
        Set<String> cartlist =new Set<String>();
        for(String i: beerList){
            cartlist.add(i);
        }
        system.debug(cartlist);
         List<Cart_Item__c> newones=new List<Cart_Item__c>();
         List<Cart_Item__c> tobeupdated=new List<Cart_Item__c>();
         Map<Id,Cart_Item__c> existingcartlist=getcartItems(cartId);
        system.debug('back to create cart items');
        for(String beer:cartlist){
            if(existingcartlist!=null && existingcartlist.containsKey(beer)){
                Cart_Item__c it =existingcartlist.get(beer);
                Cart_Item__c item=new Cart_Item__c(Id=it.id,Item_Quantity__c=it.Item_Quantity__c+1,Total_Price__c=((it.Item_Quantity__c+1)*it.Beer__r.Price__c));
                tobeupdated.add(item);
                system.debug('Beer name --> '+it.Beer__r.Name);
            }
            else{
                Decimal beerprice = getBeerPrice(beer);
                Cart_Item__c item=new Cart_Item__c(Beer__c=beer,Cart__c=cartId,Item_Quantity__c=1,Total_Price__c=beerprice);
                newones.add(item);
            }
        }
        insert newones;
        if(tobeupdated!=null && tobeupdated.size()>0){
            update tobeupdated;
        }
        return cartId;
    }
     @AuraEnabled
    public static Decimal getBeerPrice(String BeerId){
        Beer__c beer= [select Id,Price__c from Beer__c where id=:BeerId limit 1];
        return beer.Price__c;
    }
    @AuraEnabled
    public static Map<Id,Cart_Item__c> getcartItems(String cartId){
        system.debug('----cartId '+cartId);
        List<Cart_Item__c> existingcartlist= [select Id,Single_Item_price__c,Name,Beer__c,Beer__r.Name,Beer__r.Price__c,Item_Quantity__c,Total_Price__c,Cart__c from Cart_Item__c where Cart__c=:cartId];
        system.debug('----existingcartlist '+existingcartlist);
        Map<Id,Cart_Item__c> beerMap= new Map<Id,Cart_Item__c>();
        for(Cart_Item__c c : existingcartlist){
            if(!beerMap.containsKey(c.Beer__c)){
                system.debug('----c--- '+c);
                beerMap.put(c.Beer__c, c);
            }
            
        }
        system.debug('----beer Map '+beerMap);
        return beerMap;
    }
    
     @AuraEnabled
      public static String deleteCartitems(String cartItemId){
          system.debug('cartItemId----> '+cartItemId);
          Cart_Item__c delitem=[select Id,Beer__r.Name from Cart_Item__c where Id=:cartItemId];
          String name=delitem.Beer__r.Name;
          if(delitem!=null){
               delete delitem;
               return name;
          }
          return null;
         
         
      }
    @AuraEnabled
     public static void updateCartItem(String cartItemId,String Quantity){
         system.debug('quantity -->'+Quantity +'    cartItem '+ cartItemId);
         Cart_Item__c updateItem=[select Id,Beer__r.Name,Item_Quantity__c,Beer__r.Price__c from Cart_Item__c where Id=:cartItemId];
       if(String.ValueOf(Quantity).isNumeric()){
              Integer q=integer.ValueOf(Quantity.trim());
              updateItem.Item_Quantity__c=q;
             updateItem.Total_Price__c=q*updateItem.Beer__r.Price__c;
               update updateItem;
         }
         else{
              String del=deleteCartitems(cartItemId);
         }
          
     }    
      @AuraEnabled
     public static List<Address_Book__c> fetchAddress(){
         List<Address_Book__c> add=[select City__c, Country__c, Postal_Code__c, State_provinance__c, Street__c 
                               from Address_Book__c 
                               where User__c=:UserInfo.getUserId()];
         system.debug(add);
         return add;
         
     }
    
      /*  @AuraEnabled
     public static String createbeerorder(){
         Beer_Order__c beerorder=new Beer_Order__c(Billing_City__c, Billing_Country__c, Billing_State__c, Billing_Street__c,
                                  Billing_Same_as_Shipping__c, Shipping_City__c, Shipping_State__c, Shipping_Country__c, 
                                  Shipping_Street__c,Beer__c ,OrderedBy__c=UserInfo.getUserId());
                               
         insert beerorder;
         return beerorder.Id;
         
     }*/
  }