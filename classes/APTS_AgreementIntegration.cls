public class APTS_AgreementIntegration{  
    public string createRequestBody(String Oppttyid){
        
        APTS_AgreementRequest instAgreementReq   = new APTS_AgreementRequest();
        instAgreementReq.contractheaderId=Oppttyid;
        
        instAgreementReq.CONTRACT_HEADER_IN     = new List<APTS_AgreementRequest.CONTRACT_HEADER_IN>();
        instAgreementReq.CONTRACT_ITEMS_IN      = new List<APTS_AgreementRequest.CONTRACT_ITEMS_IN>();
        APTS_AgreementRequest.CONTRACT_HEADER_IN instAgreementHead     = new APTS_AgreementRequest.CONTRACT_HEADER_IN();
        APTS_AgreementRequest.CONTRACT_ITEMS_IN instAgreementItm       = new APTS_AgreementRequest.CONTRACT_ITEMS_IN();
        
        for(Account acc:[SELECT Id,name,type from Account limit 5]){
            instAgreementHead.DOC_TYPE = acc.name ;
            instAgreementHead.SALES_ORG = acc.type ;
            instAgreementReq.CONTRACT_HEADER_IN.add(instAgreementHead);
        }
        for(Opportunity opp:[SELECT Id,name,stagename from Opportunity limit 5]){
            instAgreementItm.HG_LV_ITEM = opp.name;
            instAgreementItm.ITM_NUMBER = opp.stagename;
            instAgreementReq.CONTRACT_ITEMS_IN.add(instAgreementItm);
        }
        
        System.debug(JSON.serialize(instAgreementReq.CONTRACT_HEADER_IN) );
        System.debug(instAgreementReq.CONTRACT_ITEMS_IN);
        System.debug(JSON.serialize(instAgreementReq));
        
                
     return JSON.serialize(instAgreementReq);
    }    
}