public virtual class TriggerContext {

  // standard trigger context values
  public Boolean isExecuting;
  public Boolean isAfter;
  public Boolean isBefore;
  public Boolean isDelete;
  public Boolean isInsert;
  public Boolean isUndelete;
  public Boolean isUpdate;
  public List<sObject> newList;
  public List<sObject> oldList;
  public Map<id, sObject> newMap;
  public Map<id, sObject> oldMap;
  public Integer size;
  
  // custom trigger context values
  public Boolean disableAllOrgTriggers;
  public Boolean disableObjectTriggers;
      
  // constructor
  public TriggerContext(Boolean isExecuting, Boolean isAfter, Boolean isBefore, Boolean isDelete, Boolean isInsert,
    Boolean isUndelete, Boolean isUpdate, List<sObject> newList, List<sObject> oldList, Map<id, sObject> newMap,
    Map<id, sObject> oldMap, Integer size)
  {
    this.isExecuting = isExecuting;
    this.isAfter = isAfter;
    this.isBefore = isBefore;
    this.isDelete = isDelete;
    this.isInsert = isInsert;
    this.isUndelete = isUndelete;
    this.isUpdate = isUpdate;
    this.newList = newList;
    this.oldList = oldList;
    this.newMap = newMap;
    this.oldMap = oldMap;
    this.size = size;
    this.disableAllOrgTriggers = disableAllOrgTriggers;
    this.disableObjectTriggers = disableObjectTriggers;
  }
  
  

}// end class