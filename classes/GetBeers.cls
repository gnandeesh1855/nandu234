public class GetBeers {
 @AuraEnabled
    public static List<	Beer__c> getBeerList(String SearchValueinApex){
        return [Select id,Name,brewery_id__c,Alchohol__c,Price__c  from Beer__c where Name like :'%'+SearchValueinApex+'%'];
    }
    @AuraEnabled
    public static String getBaseUrl(){
        return URL.getSalesforceBaseUrl().toExternalForm();
    }
}