public  class emailInbox {
    @AuraEnabled(cacheable=true)
    public static EmailWrapper getEmailList() {
        EmailWrapper ew = new EmailWrapper();
        list<EmailMessage> listofStanderedEmail=[select Id, Subject, FromAddress, FirstLetter_of_FromName__c, ToAddress, CreatedDate, FromName, TextBody, MessageDate, Initials__c  from EmailMessage WHERE FromName != null];
        list<cmsecureemail__Secure_EmailMessage__c> listOfCustomEmail=[Select Id, cmsecureemail__Subject__c, RecordType.Name, cmsecureemail__Reply_to__c, cmsecureemail__Recipients__c, cmsecureemail__FromAddress__c, cmsecureemail__HtmlBody__c, cmsecureemail__TextBody__c, CreatedDate from cmsecureemail__Secure_EmailMessage__c];
        ew.listofStanderedEmail = listofStanderedEmail;
        ew.listOfCustomEmail = listOfCustomEmail;
        return ew;
    }

    @AuraEnabled
    public static String updateReplyEmail(Boolean emailType, String subject, String formAddress, String toAddress, String emailBody) {
        if(emailType == true) {
            System.debug(subject);
            System.debug(formAddress);
            System.debug(toAddress);
            System.debug(emailBody);
            cmsecureemail__Secure_EmailMessage__c mail = new cmsecureemail__Secure_EmailMessage__c();
            mail.cmsecureemail__Subject__c = subject;
            mail.cmsecureemail__FromAddress__c = formAddress;
            mail.cmsecureemail__Reply_to__c = formAddress;
            mail.cmsecureemail__HtmlBody__c = emailBody;
            mail.cmsecureemail__TextBody__c = emailBody;
            mail.RecordtypeId = Schema.SObjectType.cmsecureemail__Secure_EmailMessage__c.getRecordTypeInfosByName().get('Outbound').getRecordTypeId();

            insert mail;
        } else {
            System.debug(subject);
            System.debug(formAddress);
            System.debug(formAddress);
            System.debug(emailBody);
            EmailMessage mail = new EmailMessage();
            mail.Subject = subject;
            mail.FromAddress = formAddress;
            mail.FromName = formAddress;
            mail.ToAddress = toAddress;
            mail.TextBody = emailBody;

            insert mail;
        }

        return 'Mail sent successfully';
    } 
    
    public class EmailWrapper{
        @AuraEnabled public list<EmailMessage> listofStanderedEmail;
        @AuraEnabled public list<cmsecureemail__Secure_EmailMessage__c> listOfCustomEmail;
        
    }

}