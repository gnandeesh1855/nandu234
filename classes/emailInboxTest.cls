@isTest
public class emailInboxTest {
	@isTest
    public static void updateReplyEmailTest(){
       	
        
        cmsecureemail__Secure_EmailMessage__c mail = new cmsecureemail__Secure_EmailMessage__c();
            mail.cmsecureemail__Subject__c = 'subject';
            mail.cmsecureemail__FromAddress__c =  'test@salesforce.com';
            mail.cmsecureemail__Reply_to__c =  'test1@salesforce.com';
            mail.cmsecureemail__HtmlBody__c = 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes';
            mail.cmsecureemail__TextBody__c = 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes';
            mail.RecordtypeId = Schema.SObjectType.cmsecureemail__Secure_EmailMessage__c.getRecordTypeInfosByName().get('Outbound').getRecordTypeId();

            insert mail;
               
        
         EmailMessage mail1 = new EmailMessage();
            mail1.Subject = 'subject';
            mail1.FromAddress = 'test@salesforce.com';
            mail1.FromName = 'test1@salesforce.com';
            mail1.ToAddress = 'test3@salesforce.com';
            mail1.TextBody = 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes';

            insert mail1;
        
        
    
        emailInbox eI = new emailInbox();
        emailInbox.updateReplyEmail(TRUE, 'CloudMaven' , 'test@salesforce.com', 'testreceive@salesforce.com', 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes');
      	emailInbox.getEmailList();
        
    }
    	@isTest
    public static void updateReplyEmailTest2(){
       	
        
        cmsecureemail__Secure_EmailMessage__c mail3 = new cmsecureemail__Secure_EmailMessage__c();
            mail3.cmsecureemail__Subject__c = 'subject';
            mail3.cmsecureemail__FromAddress__c =  'test@salesforce.com';
            mail3.cmsecureemail__Reply_to__c =  'test1@salesforce.com';
            mail3.cmsecureemail__HtmlBody__c = 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes';
            mail3.cmsecureemail__TextBody__c = 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes';
            mail3.RecordtypeId = Schema.SObjectType.cmsecureemail__Secure_EmailMessage__c.getRecordTypeInfosByName().get('Outbound').getRecordTypeId();

            insert mail3;
               
        
         EmailMessage mail2 = new EmailMessage();
            mail2.Subject = 'subject';
            mail2.FromAddress = 'test@salesforce.com';
            mail2.FromName = 'test1@salesforce.com';
            mail2.ToAddress = 'test3@salesforce.com';
            mail2.TextBody = 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes';

            insert mail2;
        
        
    
        emailInbox eI2 = new emailInbox();
        
        emailInbox.updateReplyEmail(FALSE, 'CloudMaven' , 'test@salesforce.com', 'testreceive@salesforce.com', 'CloudMaven continued to serve customers amid the pandemic, market volatility, and record customer volumes');
        emailInbox.getEmailList();
    }  
}